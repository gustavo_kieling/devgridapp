package com.kieling.devgridapp;

import android.util.Log;

final class Utils {
    static final String GIST_PARAMETER_KEY = "gist";
    private static final String TAG = "Utils";

    static boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (Exception e) {
            Log.e(TAG, "Error checking connection", e);
        }
        return false;
    }
}
