package com.kieling.devgridapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

class GistAdapter extends RecyclerView.Adapter<GistAdapter.MyViewHolder> {
    private Context mContext;
    private List<Gist> gistList;

    GistAdapter(Context context, List<Gist> gistList) {
        this.mContext = context;
        this.gistList = gistList;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Gist gist = gistList.get(position);
        holder.descriptionText.setText(gist.getDescription());
        holder.idText.setText(gist.getId());
        holder.commentsText.setText(mContext.getString(R.string.gist_adapter_comment, gist.getComments()));
    }

    @Override
    public int getItemCount() {
        return gistList.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_gist, parent, false);
        return new MyViewHolder(v);
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView descriptionText;
        private TextView idText;
        private TextView commentsText;

        MyViewHolder(View view) {
            super(view);
            descriptionText = (TextView) view.findViewById(R.id.rowDescription);
            idText = (TextView) view.findViewById(R.id.rowId);
            commentsText = (TextView) view.findViewById(R.id.rowComments);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(mContext, CommentActivity.class);
            intent.putExtra(Utils.GIST_PARAMETER_KEY, gistList.get(getAdapterPosition()));
            mContext.startActivity(intent);
        }
    }
}
