package com.kieling.devgridapp;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
    private ProgressDialog progressDialog;
    private TextView loginErrorMessageView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginErrorMessageView = (TextView) findViewById(R.id.loginErrorMessage);
        loginErrorMessageView.setVisibility(View.GONE);
    }

    public void loginButtonCLicked(View view) throws IOException {
        EditText usernameField = (EditText) findViewById(R.id.loginFieldUsername);
        EditText passwordField = (EditText) findViewById(R.id.loginFieldPassword);
        EditText targetUserField = (EditText) findViewById(R.id.loginFieldTargetUser);
        String username = usernameField.getText().toString();
        if (username.isEmpty() || (username.length() > 39)) {
            loginErrorMessageView.setText(getString(R.string.login_error_message_invalid_field, getString(R.string.main_field_username)));
            loginErrorMessageView.setVisibility(View.VISIBLE);
            return;
        }
        /*
         * ^             # start-of-string
         * (?=.*[0-9])   # a digit must occur at least once
         * (?=.*[a-z])   # a lower case letter must occur at least once
         * (?=.*[A-Z])   # an upper case letter must occur at least once
         * (?=\S+$)      # no whitespace allowed in the entire string
         * .{7,}         # anything, at least seven places though
         * $             # end-of-string
         */
        String pattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{7,}$";
        String password = passwordField.getText().toString();
        if (!password.matches(pattern)) {
            loginErrorMessageView.setText(getString(R.string.login_error_message_invalid_field, getString(R.string.main_field_password)));
            loginErrorMessageView.setVisibility(View.VISIBLE);
            return;
        }
        String targetUser = targetUserField.getText().toString();
        if (targetUser.isEmpty() || (targetUser.length() > 39)) {
            loginErrorMessageView.setText(getString(R.string.login_error_message_invalid_field, getString(R.string.main_field_target_user)));
            loginErrorMessageView.setVisibility(View.VISIBLE);
            return;
        }

        RestClient.init(this, username, password, targetUser);
        if (Utils.isOnline()) {
            progressDialog = ProgressDialog.show(this, null, "Loading...", true);
            RestClient.getToken(this);
        } else {
            loginErrorMessageView.setText(R.string.login_error_message_no_connection);
            loginErrorMessageView.setVisibility(View.VISIBLE);
        }
    }

    public void loginFailCallback(String errorMessage) {
        dismissDialog();
        loginErrorMessageView.setText(getString(R.string.login_error_message_fail, errorMessage));
        loginErrorMessageView.setVisibility(View.VISIBLE);
    }

    public void dismissDialog() {
        if ((progressDialog != null) && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
