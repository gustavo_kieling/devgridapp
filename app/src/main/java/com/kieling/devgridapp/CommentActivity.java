package com.kieling.devgridapp;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class CommentActivity extends AppCompatActivity {
    private static final String TAG = "CommentActivity";
    private String mId;
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Gist gist = (Gist) getIntent().getSerializableExtra(Utils.GIST_PARAMETER_KEY);
        Log.d(TAG, "Commenting in gist " + gist.getId());
        mId = gist.getId();

        TextView commentGistId = (TextView) findViewById(R.id.commentGistId);
        commentGistId.setText(getString(R.string.comment_title, gist.getId()));
    }

    public void commentButtonCLicked(View view) {
        mProgress = ProgressDialog.show(this, null, getString(R.string.comment_dialog_title), true);
        TextView tvComment = (TextView) findViewById(R.id.commentGistText);
        String message = tvComment.getText().toString();
        RestClient.postGistComment(this, mId, message);
    }

    void updateCallback(boolean success) {
        mProgress.dismiss();
        new AlertDialog.Builder(this)
                .setTitle(null)
                .setMessage(success ? R.string.comment_callback_success : R.string.comment_callback_fail)
                .setNeutralButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        finish();
                    }
                })
                .show();
    }
}
