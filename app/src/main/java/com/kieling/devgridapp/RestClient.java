package com.kieling.devgridapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Debug;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

final class RestClient {
    private static final int PORT = 80;
    private static final String TAG = "RestClient";
    private static final String BASE_URL = "https://api.github.com";
    private static final String PREFERENCES_TOKEN_KEY = "token";
    private static final AsyncHttpClient mClient = new AsyncHttpClient(PORT);
    private static String mTargetUser;
    private static SharedPreferences mSharedPref;

    static void init(MainActivity activity, String username, String password, String targetUser) throws IOException {
        mTargetUser = targetUser;
        mSharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        String oauthAccessToken = null;//mSharedPref.getString(PREFERENCES_TOKEN_KEY, null);
        Log.d(TAG, String.format("I have token '%s' in SP", oauthAccessToken));

        String encodedAuthorization;
        if (oauthAccessToken != null) {
            encodedAuthorization = "token " + oauthAccessToken;
        } else {
            if (password != null) {
                String authorization = String.format("%s:%s", username, password);
                String charsetName = StandardCharsets.UTF_8.name();
                encodedAuthorization = "Basic " + new String(Base64.encodeBase64(authorization.getBytes(charsetName)), charsetName);
            } else {// anonymous access
                encodedAuthorization = null;
            }
        }
        mClient.addHeader("Authorization", encodedAuthorization);
        mClient.addHeader("User-Agent", "DevgridApp");
        //mClient.addHeader("X-OAuth-Scopes", "gist");
        //mClient.addHeader("X-Accepted-OAuth-Scopes", "user");
    }

    private static String getAbsoluteUrl(String method) {
        return BASE_URL + method;
    }

    static void getToken(final MainActivity activity) {
        if (Debug.isDebuggerConnected()) {
            Debug.waitForDebugger();
        }
        JsonHttpResponseHandler jsonHttpResponseHandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                if (Debug.isDebuggerConnected()) {
                    Debug.waitForDebugger();
                }
                if (response == null) {
                    Log.d(TAG, "getToken onSuccess response is null");
                }
                try {
                    if (response != null) {
                        String token = response.getString("token");
                        Log.d(TAG, "Token received: " + token);
                        SharedPreferences.Editor editor = mSharedPref.edit();
                        editor.putString(PREFERENCES_TOKEN_KEY, token);
                        // Log.d(TAG, "client_id received: " + clientId);
                        // String clientId = response.getString("client_id");
                        // editor.putString(PREFERENCES_CLIENT_ID_KEY, clientId);
                        // Log.d(TAG, "client_secret received: " + clientSecret);
                        // String clientSecret = response.getString("client_secret");
                        // editor.putString(PREFERENCES_CLIENT_SECRET_KEY, clientSecret);
                        editor.apply();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Error getting token", e);
                }
                Log.d(TAG, "getToken onSuccess");
                activity.dismissDialog();

                Intent intent = new Intent(activity, GistsActivity.class);
                activity.startActivity(intent);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if (Debug.isDebuggerConnected()) {
                    Debug.waitForDebugger();
                }
                Log.e(TAG, "getToken failure: ", throwable);
                activity.loginFailCallback(throwable.getMessage());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (Debug.isDebuggerConnected()) {
                    Debug.waitForDebugger();
                }
                if (errorResponse == null) {
                    Log.e(TAG, String.format("getToken failure 2. statusCode: %d; errorResponse: null", statusCode), throwable);
                    activity.loginFailCallback(throwable.getMessage());
                } else {
                    Log.e(TAG, String.format("getToken failure 2. statusCode: %d; errorResponse: %s", statusCode, errorResponse), throwable);
                    try {
                        activity.loginFailCallback(errorResponse.getString("message"));
                    } catch (JSONException e) {
                        Log.e(TAG, "Error getting error message from response");
                        activity.loginFailCallback(throwable.getMessage());
                    }
                }
            }
        };

        if (Debug.isDebuggerConnected()) {
            Debug.waitForDebugger();
        }
        String method = "/authorizations";
        try {
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("scopes", new JSONArray(new String[]{"gist"}));
            jsonParams.put("note", "DevgridApp");
            StringEntity entity = new StringEntity(jsonParams.toString(), StandardCharsets.UTF_8);
            mClient.post(activity, getAbsoluteUrl(method), entity, "application/json", jsonHttpResponseHandler);
            //mClient.setBasicAuth(username, password);
            Log.d(TAG, String.format("Posting getToken: %s", jsonParams));
        } catch (Exception e) {
            Log.e(TAG, "getToken error build JSON request", e);
        }
    }

    static void getAllGists(final GistsActivity activity) {
        JsonHttpResponseHandler jsonHttpResponseHandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                Log.d(TAG, String.format("Received response from all gists: %s", response));
                JSONObject jsonObject;
                Gist gist;
                ArrayList<Gist> gists = new ArrayList<>();
                for (int i = 0; i < response.length(); i++) {
                    try {
                        jsonObject = (JSONObject) response.get(i);
                        gist = new Gist(jsonObject.getString("id"), jsonObject.getString("description"), jsonObject.getInt("comments"));
                        gists.add(gist);
                    } catch (JSONException e) {
                        Log.e(TAG, "Error parsing response", e);
                    }
                }
                activity.updateEntries(gists);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.e(TAG, String.format("loadContacts failure 1. statusCode: %d; responseString: %s", statusCode, responseString), throwable);
                activity.updateEntries(new ArrayList<Gist>());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    Log.e(TAG, String.format("loadContacts failure 2. statusCode: %d; errorResponse: %s", statusCode, errorResponse.toString()), throwable);
                } else {
                    Log.e(TAG, String.format("loadContacts failure 2. statusCode: %d; errorResponse: null", statusCode), throwable);
                }
                activity.updateEntries(new ArrayList<Gist>());
            }
        };

        String method = String.format("/users/%s/gists", mTargetUser);
        RequestParams params = new RequestParams();
        params.add("since", "2000-01-01T00:00:000");
        mClient.get(getAbsoluteUrl(method), params, jsonHttpResponseHandler);
        Log.d(TAG, "Returning getAllGists");
    }

    static void postGistComment(final CommentActivity activity, String gistId, String comment) {
        JsonHttpResponseHandler jsonHttpResponseHandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d(TAG, String.format("Received response from postGistComment: %s", response));
                activity.updateCallback(true);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.e(TAG, String.format("postGistComment failure 1. statusCode: %d; responseString: %s", statusCode, responseString), throwable);
                activity.updateCallback(false);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    Log.e(TAG, String.format("postGistComment failure 2. statusCode: %d; errorResponse: %s", statusCode, errorResponse.toString()), throwable);
                } else {
                    Log.e(TAG, String.format("postGistComment failure 2. statusCode: %d; errorResponse: null", statusCode), throwable);
                }
                activity.updateCallback(false);
            }
        };

        String method = String.format(Locale.getDefault(), "/gists/%s/comments", gistId);
        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("body", comment);
            StringEntity entity = new StringEntity(jsonParams.toString(), StandardCharsets.UTF_8);
            mClient.post(activity, getAbsoluteUrl(method), entity, "application/json", jsonHttpResponseHandler);
            Log.d(TAG, "Returning postGistComment");
        } catch (JSONException e) {
            Log.e(TAG, "Error adding parameters in postGistComment", e);
        }
    }
}
