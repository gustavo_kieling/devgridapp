package com.kieling.devgridapp;

import java.io.Serializable;

class Gist implements Serializable {
    private String id;
    private String description;
    private int comments;

    Gist(String id, String description, int comments) {
        this.id = id;
        this.description = description;
        this.comments = comments;
    }

    String getId() {
        return id;
    }

    String getDescription() {
        return description;
    }

    int getComments() {
        return comments;
    }
}
