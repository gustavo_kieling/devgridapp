package com.kieling.devgridapp;

import android.Manifest;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class GistsActivity extends AppCompatActivity {
    private static final String TAG = "GistsActivity";
    private static final int PHOTO_REQUEST = 10;
    private static final int REQUEST_WRITE_PERMISSION = 20;
    private static final String SAVED_INSTANCE_URI = "uri";
    private static final String SAVED_INSTANCE_RESULT = "result";
    private String scanResult;
    private BarcodeDetector detector;
    private Uri imageUri;
    private TextView errorMessageView;
    private List<Gist> gistList = new ArrayList<>();
    private GistAdapter mAdapter;
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gists);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mProgress = ProgressDialog.show(this, null, getString(R.string.gists_dialog_title), true);

        errorMessageView = (TextView) findViewById(R.id.errorMessage);
        errorMessageView.setText(null);

        //noinspection unchecked
        RecyclerView rv = (RecyclerView) findViewById(R.id.recyclerView);
        mAdapter = new GistAdapter(getBaseContext(), gistList);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(llm);
        rv.setAdapter(mAdapter);

        RestClient.getAllGists(this);
        if (savedInstanceState != null) {
            String uriString = savedInstanceState.getString(SAVED_INSTANCE_URI);
            if (uriString != null) {
                imageUri = Uri.parse(uriString);
            }
        }
        Button qrCodeButton = (Button) findViewById(R.id.qrCodeButton);
        qrCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(GistsActivity.this, new
                        String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
            }
        });

        detector = new BarcodeDetector.Builder(getApplicationContext())
                .setBarcodeFormats(Barcode.DATA_MATRIX | Barcode.QR_CODE)
                .build();
        if (!detector.isOperational()) {
            Log.d(TAG, "Could not set up the detector!");
            errorMessageView.setText(R.string.gists_not_operational);
        }
    }

    void updateEntries(List<Gist> entries) {
        Log.d(TAG, String.format("Received %d items", entries.size()));
        gistList.clear();
        gistList.addAll(entries);
        mAdapter.notifyDataSetChanged();
        Log.d(TAG, String.format("My gist list has now %d items", mAdapter.getItemCount()));
        mProgress.dismiss();
    }

    //region QR Code
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_WRITE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takePicture();
                } else {
                    Toast.makeText(this, R.string.gists_scanning_permission_denied, Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PHOTO_REQUEST && resultCode == RESULT_OK) {
            launchMediaScanIntent();
            try {
                Bitmap bitmap = decodeBitmapUri(this, imageUri);
                if (detector.isOperational() && (bitmap != null)) {
                    Frame frame = new Frame.Builder().setBitmap(bitmap).build();
                    SparseArray<Barcode> barcodes = detector.detect(frame);
                    for (int index = 0; index < barcodes.size(); index++) {
                        Barcode code = barcodes.valueAt(index);
                        scanResult = code.displayValue;

                        //Required only if you need to extract the type of barcode
                        int type = barcodes.valueAt(index).valueFormat;
                        switch (type) {
                            case Barcode.CONTACT_INFO:
                                Log.i(TAG, "result: " + code.contactInfo.title);
                                break;
                            case Barcode.EMAIL:
                                Log.i(TAG, "result: " + code.email.address);
                                break;
                            case Barcode.ISBN:
                                Log.i(TAG, "result: " + code.rawValue);
                                break;
                            case Barcode.PHONE:
                                Log.i(TAG, "result: " + code.phone.number);
                                break;
                            case Barcode.PRODUCT:
                                Log.i(TAG, "result: " + code.rawValue);
                                break;
                            case Barcode.SMS:
                                Log.i(TAG, "result: " + code.sms.message);
                                break;
                            case Barcode.TEXT:
                                Log.i(TAG, "result: " + code.rawValue);
                                break;
                            case Barcode.URL:
                                Log.i(TAG, "result: " + code.url.url);
                                break;
                            case Barcode.WIFI:
                                Log.i(TAG, "result: " + code.wifi.ssid);
                                break;
                            case Barcode.GEO:
                                Log.i(TAG, "result: " + code.geoPoint.lat + ":" + code.geoPoint.lng);
                                break;
                            case Barcode.CALENDAR_EVENT:
                                Log.i(TAG, "result: " + code.calendarEvent.description);
                                break;
                            case Barcode.DRIVER_LICENSE:
                                Log.i(TAG, "result: " + code.driverLicense.licenseNumber);
                                break;
                            default:
                                Log.i(TAG, "result: " + code.rawValue);
                                break;
                        }
                    }
                    if (barcodes.size() == 0) {
                        Log.d(TAG, "Scan Failed: Found nothing to scan");
                        errorMessageView.setText(R.string.gists_scanning_no_result);
                    } else {
                        Log.i(TAG, "Scan result: " + scanResult);
                        Intent intent = new Intent(getBaseContext(), CommentActivity.class);
                        Gist gist = new Gist(scanResult, "", 0);
                        intent.putExtra(Utils.GIST_PARAMETER_KEY, gist);
                        startActivity(intent);
                    }
                } else {
                    Log.d(TAG, "Could not set up the detector!");
                }
            } catch (Exception e) {
                errorMessageView.setText(R.string.gists_scanning_error);
                Log.e(TAG, "Error loading image", e);
            }
        }
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = new File(Environment.getExternalStorageDirectory(), "picture.jpg");
        imageUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", photo);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, PHOTO_REQUEST);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (imageUri != null) {
            outState.putString(SAVED_INSTANCE_URI, imageUri.toString());
            outState.putString(SAVED_INSTANCE_RESULT, scanResult);
        }
        super.onSaveInstanceState(outState);
    }

    private void launchMediaScanIntent() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(imageUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private Bitmap decodeBitmapUri(Context ctx, Uri uri) throws FileNotFoundException {
        int targetW = 600;
        int targetH = 600;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;

        return BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
    }
    //endregion
}
